package main.intWithoutZero;

public class IntWithoutZero {

    public static int noBoringZeros(int n) {

        String intAsString = String.valueOf(n);

        if(!recurrence(intAsString).equals("")){
            return Integer.parseInt(recurrence(intAsString));
        }

        return 0;
    }

    private static String recurrence(String intAsString) {

        char lastChar = '$';
        if (intAsString.length() > 0) {
            lastChar = intAsString.charAt(intAsString.length() - 1);

        }
        String subString;


        if (lastChar == '0') {
            subString = intAsString.substring(0, intAsString.length() - 1);
            return recurrence(subString);
        }

        return intAsString;
    }

}
