package main.removeExclemationMarks;

/*Description:
        Remove n exclamation marks in the sentence from left to right. n is positive integer.

        Examples
        remove("Hi!",1) === "Hi"
        remove("Hi!",100) === "Hi"
        remove("Hi!!!",1) === "Hi!!"
        remove("Hi!!!",100) === "Hi"
        remove("!Hi",1) === "Hi"
        remove("!Hi!",1) === "Hi!"
        remove("!Hi!",100) === "Hi"
        remove("!!!Hi !!hi!!! !hi",1) === "!!Hi !!hi!!! !hi"
        remove("!!!Hi !!hi!!! !hi",3) === "Hi !!hi!!! !hi"
        remove("!!!Hi !!hi!!! !hi",5) === "Hi hi!!! !hi"
        remove("!!!Hi !!hi!!! !hi",100) === "Hi hi hi"*/

public class RemoveExclamationMarks {

    public static String remove(String s, int n){

        String result = "";
        for(int i=0; i < n; i++){
            if (s.contains("!")){
                s = s.replaceFirst("!","");
            }
        }
        return s;
    }



}
